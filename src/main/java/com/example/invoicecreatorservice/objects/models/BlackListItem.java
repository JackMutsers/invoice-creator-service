package com.example.invoicecreatorservice.objects.models;

import com.example.invoicecreatorservice.objects.data_transfer_objects.BlackListItemDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@SuppressWarnings("WeakerAccess")
@AllArgsConstructor
@Entity
@Getter
@Setter
public class BlackListItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String ipAdress;
    private String addDate;

    public BlackListItem() {

    }

    public BlackListItem(BlackListItemDTO itemDTO) {
        this.ipAdress = itemDTO.getIpAddress();
        this.addDate = itemDTO.getAddDate();
    }
}
