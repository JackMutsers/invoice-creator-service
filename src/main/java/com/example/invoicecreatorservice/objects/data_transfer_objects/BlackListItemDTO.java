package com.example.invoicecreatorservice.objects.data_transfer_objects;

import com.example.invoicecreatorservice.objects.models.BlackListItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BlackListItemDTO {
    private String ipAddress;
    private String addDate;

    public BlackListItemDTO(BlackListItem listItem) {
        this.ipAddress = listItem.getIpAdress();
        this.addDate = listItem.getAddDate();
    }

    public BlackListItemDTO(){}
}
