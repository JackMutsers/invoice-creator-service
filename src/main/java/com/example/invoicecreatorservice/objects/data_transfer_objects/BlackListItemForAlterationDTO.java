package com.example.invoicecreatorservice.objects.data_transfer_objects;

import lombok.Getter;

@Getter
public class BlackListItemForAlterationDTO {
    private int id;
    private String ipAaddress;
    private String addDate;

    public BlackListItemForAlterationDTO(String ipAaddress, String addDate) {
        this.ipAaddress = ipAaddress;
        this.addDate = addDate;
    }

    public BlackListItemForAlterationDTO(){}
}
