package com.example.invoicecreatorservice.contracts.services;

import com.example.invoicecreatorservice.objects.data_transfer_objects.BlackListItemDTO;
import com.example.invoicecreatorservice.objects.data_transfer_objects.UserForAlterationDTO;

import java.util.List;

public interface IBlackListService {
    BlackListItemDTO getBlackListItem(int id);
    List<BlackListItemDTO> getBlackList(int id);
    boolean DeleteBlackListItem(int id);
    BlackListItemDTO addBlackListItem(BlackListItemDTO itemDTO);
}
