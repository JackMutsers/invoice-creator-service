package com.example.invoicecreatorservice;

import com.example.invoicecreatorservice.contracts.services.StorageService;
import com.example.invoicecreatorservice.helpers.properties.StorageProperties;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@EnableAsync
@EnableConfigurationProperties(StorageProperties.class)
public class InvoiceCreatorServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(InvoiceCreatorServiceApplication.class, args);
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins(
//                        "intranet.jack.mylocal",
//                        "intranet.jack.mylocal:443",
//                        "intranet.jack.mylocal:9090",
                        "http://intranet.jack.mylocal",
//                        "http://intranet.jack.mylocal:80",
//                        "http://intranet.jack.mylocal:443",
                        "https://intranet.jack.mylocal",
//                        "https://intranet.jack.mylocal/",
//                        "https://intranet.jack.mylocal:80/",
//                        "https://intranet.jack.mylocal:443",
//                        "https://intranet.jack.mylocal:443/",
                        "http://localhost:4200",
                        "https://localhost:4200"
//                        "localhost",
//                        "http://localhost",
//                        "http://localhost:80",
//                        "https://localhost",
//                        "https://localhost:443",
//                        "10.10.10.101",
//                        "http://10.10.10.101",
//                        "http://10.10.10.101:80",
//                        "https://10.10.10.101",
//                        "https://10.10.10.101:443"
                );
            }
        };
    }

    @Bean
    CommandLineRunner init(StorageService storageService) {
        return args -> storageService.init();
    }
}
