package com.example.invoicecreatorservice.repositories;

import com.example.invoicecreatorservice.objects.models.BlackListItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BlackListRepo extends CrudRepository<BlackListItem, Integer> {
    BlackListItem findById(int id);
    List<BlackListItem> findAllById(int id);
}
