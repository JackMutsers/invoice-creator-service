package com.example.invoicecreatorservice.services;

import com.example.invoicecreatorservice.contracts.services.IBlackListService;
import com.example.invoicecreatorservice.contracts.services.IUserService;
import com.example.invoicecreatorservice.helpers.logger.LoggerService;
import com.example.invoicecreatorservice.objects.data_transfer_objects.BlackListItemDTO;
import com.example.invoicecreatorservice.objects.data_transfer_objects.CompanyDTO;
import com.example.invoicecreatorservice.objects.data_transfer_objects.UserDTO;
import com.example.invoicecreatorservice.objects.data_transfer_objects.UserForAlterationDTO;
import com.example.invoicecreatorservice.objects.models.BlackListItem;
import com.example.invoicecreatorservice.objects.models.Company;
import com.example.invoicecreatorservice.objects.models.User;
import com.example.invoicecreatorservice.repositories.BlackListRepo;
import com.example.invoicecreatorservice.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static com.example.invoicecreatorservice.helpers.tools.Helper.emptyIfNull;

@Service
public class BlackListService implements IBlackListService {
    @Autowired
    private BlackListRepo blackListRepo;

    public BlackListItemDTO getBlackListItem(int id) {
        BlackListItem item = blackListRepo.findById(id);

        if(item == null){return new BlackListItemDTO();}

        return new BlackListItemDTO(item);
    }

    public List<BlackListItemDTO> getBlackList(int id) {
        List<BlackListItem> blackList = blackListRepo.findAllById(id);
        return this.convertListToDTO(blackList);
    }

    public boolean DeleteBlackListItem(int id) {
        try{
            blackListRepo.deleteById(id);
            return true;
        }catch (Exception ex){
            LoggerService.warn(ex.getMessage());
            return false;
        }
    }

    public BlackListItemDTO addBlackListItem(BlackListItemDTO itemDTO) {
        try{
            BlackListItem item = new BlackListItem(itemDTO);

            // set id to 0 to prevent update of existing record on create
            item.setId(0);

            BlackListItem newObject = blackListRepo.save(item);
            return new BlackListItemDTO(newObject);
        }catch (Exception ex){
            LoggerService.warn(ex.getMessage());
            return null;
        }
    }

    private List<BlackListItemDTO> convertListToDTO(List<BlackListItem> list){
        List<BlackListItemDTO> blackList = new ArrayList<>();

        for(BlackListItem item : emptyIfNull(list)){
            blackList.add(new BlackListItemDTO(item));
        }

        return blackList;
    }
}
